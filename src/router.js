import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from './views/Dashboard.vue'
import Detail from './views/DetailFilm.vue'

Vue.use(Router)
const login = () => import('./views/Login.vue')

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      component: Dashboard
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/detail/:id',
      name: 'detail',
      component: Detail,
    },
  ]
})
